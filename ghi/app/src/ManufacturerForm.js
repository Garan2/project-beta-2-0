import React, {useState} from 'react';

const ManufacturerForm = () => {
    const [manufacturerName, setManufacturerName] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        const newManufacturer = {"name": manufacturerName}
        const manufacturerURL = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newManufacturer),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(manufacturerURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setManufacturerName('');
            })
            .catch(e => console.error("error", e))

    }

    const handleNameChange = (event) => {
        const value =event.target.value;
        setManufacturerName(value);
    }



    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4 forms">
            <h1 className='large-heading-dark'>Create a manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={manufacturerName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-dark buttons">Create</button>
              <a href="/manufacturers/" className="btn btn-outline-dark buttons-2" role="button">Back to Manufacturer List
            </a>
            </form>
          </div>
        </div>
      </div>
    );


}


export default ManufacturerForm;
