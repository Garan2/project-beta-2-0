import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <button
          className="navbar-toggler collapsed"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
            {/* Register A Vehicle Dropdown start*/}
            <div className='dropdown'>
              <button
              className='btn btn-dark dropdown-toggle'
              type='button'
              id='dropdownMenuButton'
              data-bs-toggle="dropdown"
              aria-haspopup='true'
              aria-expanded='false'>
              Register Vehicle
              </button>
              <div className='dropdown-menu dropdown-menu-dark bg-dark ' aria-labelledby='dropdownMenuButton'>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/manufacturers">Manufacturers</NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/models">Models</NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/automobiles">Automobiles</NavLink>
                </li>
              </div>
            </div>
            {/* Register A Vehicle Dropdown end*/}
            {/* Sales Dropdown start*/}
            <div className='dropdown'>
              <button
              className='btn btn-dark dropdown-toggle'
              type='button'
              id='dropdownMenuButton'
              data-bs-toggle="dropdown"
              aria-haspopup='true'
              aria-expanded='false'>
              Sales
              </button>
              <div className='dropdown-menu dropdown-menu-dark bg-dark' aria-labelledby='dropdownMenuButton'>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/customers/new">Add a Customer</NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/salesteam/new">Add a Salesperson</NavLink>
                </li>
                <li className='dropdown-item'>
                  <NavLink className="nav-link text-white" to="/autotechnician/new">Add an Auto technician</NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/salesrecords">Salesrecords</NavLink>
                </li>
              </div>
            </div>
            {/* Vehicle Dropdown end*/}
            {/* Service Appointments Dropdown start*/}
            <div className='dropdown'>
              <button
              className='btn btn-dark dropdown-toggle'
              type='button'
              id='dropdownMenuButton'
              data-bs-toggle="dropdown"
              aria-haspopup='true'
              aria-expanded='false'>
              Service Appointments
              </button>
              <div className='dropdown-menu dropdown-menu-dark bg-dark' aria-labelledby='dropdownMenuButton'>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/serviceappointment/new">Create Service Appointment</NavLink>
                </li>
                <li className="dropdown-item">
                  <NavLink className="nav-link text-white" to="/serviceappointments/">Service Appointments</NavLink>
                </li>
                <li className='dropdown-item '>
                  <NavLink className="nav-link text-white" to="servicehistory/">Service History</NavLink>
                </li>
              </div>
              {/* Service Appointments Dropdown end*/}
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
