import './app.css'

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold large-heading">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <h3 className='small-heading'>
          The premiere solution for automobile dealership
          management
        </h3>
        </div>
      </div>
  );
}

export default MainPage;
